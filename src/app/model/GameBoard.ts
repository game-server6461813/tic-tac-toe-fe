import {Player} from './Player';
export class GameBoard {
    public  board : string[] = ['', '', '', '','','','','',''];
    public  players : Player[] = [];
    public winner: number = -1; 
    public gameId: string = ""; 
    
    constructor(board : string[], players : Player[], winner: number, gameId: string) {
        //specify your own constructor logic
        this.board=board;
        this.players=players;
        this.winner=winner;
        this.gameId=gameId;
    }
}