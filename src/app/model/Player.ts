export class Player {
     public userName: string = "";
     public turn: number = 0;
     constructor(userName: string, turn: number) {
        //specify your own constructor logic
        this.userName=userName;
        this.turn=turn;
    }
}