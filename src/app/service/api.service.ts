import { Injectable } from '@angular/core';
import { HttpClient,HttpHeaders } from '@angular/common/http';


const headerDict = {
  
}

const requestOptions = {                                                                                                                                                                                 
  headers: new Headers(headerDict), 
};

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(private http: HttpClient) { }

  rootURL = 'http://localhost:9000/api/';
  getLobby(userName: string) {
    return this.http.get(this.rootURL+ 'lobby/'+ userName, {responseType: 'text'});
  }

  getGame(id: string, prevTurn: number, nextTurn: number, position: number, value: string, shouldUpdate: boolean) {
    return this.http.get(this.rootURL+ 'getgame', {params: {
      'gameId': id,
      'prevTurn': prevTurn,
      'nextTurn': nextTurn,
      'position': position,
      'value': value,
      'shouldUpdate': shouldUpdate

    }, responseType: 'text'});
  }
}
