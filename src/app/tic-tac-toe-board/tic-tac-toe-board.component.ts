import { Component } from '@angular/core';
import { takeUntil } from 'rxjs';
import { GameBoard } from '../model/GameBoard';
import { ApiService } from '../service/api.service';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';

export interface Tile {
  color: string;
  cols: number;
  rows: number;
  text: string;
}

@Component({
  selector: 'app-tic-tac-toe-board',
  templateUrl: './tic-tac-toe-board.component.html',
  styleUrls: ['./tic-tac-toe-board.component.css']
})



export class TicTacToeBoardComponent {
  public value = '';
  public  data: string[] = ['', '', '', '','','','','',''];
  private apiService: ApiService
  private gID = ''
  private myTurn = 0
  private nextTurn = 0
  public lockBoard = true
  public hideStart = false
  public playerName1 = 'melanie'
  public playerName2 = 'ebe'


  constructor (private api: ApiService){
    this.apiService = api
  }

  private updateData(i: number) {
    this.data[i] = this.value
  }

  isDisabled(i: number) {
    return this.data[i] == '' ? false : true
  }

  lobby(userName: string) {
    this.data = ['', '', '', '','','','','',''];
    this.apiService.getLobby(userName).subscribe( data => {
      let jsonObj = JSON.parse(data);
      let gameBoard = jsonObj as GameBoard;
      this.gID = gameBoard.gameId
      this.myTurn = gameBoard.players.length -1
      this.nextTurn = gameBoard.players.length == 2 ? 0 : 1
      this.value = this.myTurn == 0 ? 'O' : 'X'
      if (this.myTurn == 0) {
        this.lockBoard = true
       // this.playerName1 = gameBoard.players[0].userName
        this.getBoard( 0, false)

      } else {
        console.log("your move first ..... "+ this.gID)
        this.lockBoard = false
       // this.playerName1 = gameBoard.players[0].userName
       // this.playerName2 = gameBoard.players[1].userName
      }
      this.hideStart = true
    },
     err => {

     }) 
  }
  

  getBoard( position: number, shouldUpdate: boolean) {
    if (shouldUpdate) {
    console.log("next player turn. waiting on player... ")
    this.updateData(position)
    }
    this.lockBoard = true
    this.apiService.getGame(this.gID,this.myTurn,this.nextTurn,position,this.value, shouldUpdate).subscribe( data => {
      let jsonObj = JSON.parse(data);
      let gameBoard = jsonObj as GameBoard;
      this.setData(gameBoard.board)
      this.lockBoard = false
      if(!shouldUpdate) {
       // this.playerName2 = gameBoard.players[0].userName
       // this.playerName2 = gameBoard.players[1].userName
      }
      console.log("winner")
      if(gameBoard.winner != null ) {
        alert(gameBoard.players[gameBoard.winner].userName+ ": Win!")
        this.hideStart = false
      }
      
      
    },
     err => {

     }) 
  }

  private setData(tttdata:  string[])  {
    this.data[0] = tttdata[0]
    this.data[1] = tttdata[1]
    this.data[2] = tttdata[2]
    this.data[3] = tttdata[3]
    this.data[4] = tttdata[4]
    this.data[5] = tttdata[5]
    this.data[6] = tttdata[6]
    this.data[7] = tttdata[7]
    this.data[8] = tttdata[8]
  }

  ngOnInit() {
   this.data = ['', '', '', '','','','','',''];
   this.gID = ''
  }

}
